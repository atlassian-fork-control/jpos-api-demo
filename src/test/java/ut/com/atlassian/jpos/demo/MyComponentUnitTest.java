package ut.com.atlassian.jpos.demo;

import org.junit.Test;
import com.atlassian.jpos.demo.api.MyPluginComponent;
import com.atlassian.jpos.demo.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}